# aplicacionweb

Aplicación web básica y de ejemplo siguiendo el patrón de arquitectura MVC (Modelo-Vista-Controlador) mediante el uso de
clases de Java (Modelos), JavaServer Pages (Vistas) y Java Servlets (Controladores).

## Requerimientos

Se requiere la instalación de:

-  Docker: para la creación del entorno local de ejecución de la aplicación mediante Apache Tomcat.
-  GNU Make: para la programación y ejecución de tareas.
-  Maven: para la gestión del proyecto y sus dependencias.

## Preparación

Una vez clonado el proyecto y estando en su interior:

1. Descarga la imagen y crea el contenedor:

```shell
$ make docker-image
$ make docker-container
```

2. Valida la disponibilidad de dependencias y verifica la ejecución de pruebas:

```shell
$ make mvn-validate
$ make mvn-verify
```

3. Estando el contenedor en ejecución, empaqueta el proyecto en formato WAR y realiza el deployment del mismo
   al contenedor:

```shell
$ make mvn-deploy
```

Al terminar podrás acceder a [http://localhost:8080](http://localhost:8080).

## Uso

Puedes editar o crear nuevos modelos, vistas y controladores tantos como te sean necesarios tomando en cuenta
la organización del proyecto:

-  Los modelos (_clases de Java_) residen dentro de `src/main/java/aplicacionweb/modelos`.
-  Las vistas (_JavaServer Pages_) residen dentro de `src/main/webapp/WEB-INF/vistas`.
-  Los controladores (_Java Servlets_) residen dentro de `src/main/java/aplicacionweb/controladores`.

## Licencia

[MIT](https://choosealicense.com/licenses/mit/)
