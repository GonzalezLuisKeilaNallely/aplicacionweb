<%@page contentType="text/html" pageEncoding="UTF-8"%>
  head><link rel="stylesheet" type="text/css" href="../assets/bulma/css/bulma.min.css"></head>
<body>
  <section class="hero is-fullheight is-dark">
    <div class="hero-head">
      <nav class="navbar">
        <div class="container">
          <div id="navbarMenu" class="navbar-menu">
            <div class="navbar-end">
              <span class="navbar-item">
                <a class="button is-primary" href="/inicio">
                  <span class="icon">
                    <i class="fa fa-home"></i>
                  </span>
                  <span>Servlet de Inicio</span>
                </a>
              </span>
              <span class="navbar-item">
                <a class="button is-success" href="https://gitlab.com/GonzalezLuisKeilaNallely/aplicacionweb">
                  <span class="icon">
                    <i class="fa fa-github"></i>
                  </span>
                  <span>Ver código fuente</span>
                </a>
              </span>
            </div>
          </div>
        </div>
      </nav>
      </div>
      <div class="hero-body ">
        <div class="container is-primary  has-text-centered" >
          <div class="column is-6 is-offset-3">
            <h1 class="title">
              Aplicación Web
            </h1>
            <h2 class="subtitle">
              Hecho por Gonzalez Luis Keila Nallely
            </h2>
          </div>
        </div>
      </div>
  </section>
  <script src="js/main.js?v6" async defer></script>
</body>

