<%@page contentType="text/html" pageEncoding="UTF-8"%>
<head><link rel="stylesheet" type="text/css" href="../assets/bulma/css/bulma.min.css"></head>
<body>
  <nav class="navbar" role="navigation" aria-label="main navigation">
    <div class="container">
    <div class="navbar-brand">
      <a class="navbar-item" href="">
        <b></b>
      </a>
  
      <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>
    </div>
  
    <div id="navbarBasicExample" class="navbar-menu">
      <div class="navbar-start">
        <a href="https://unamcriptografia.wordpress.com/category/grupos/" class="navbar-item">
          Fuente
        </a>
      </div>
    </div>
  </div>
  </nav>
  <section class="hero is-small is-primary is-bold">
    <div class="hero-body">
      <div class="container">
        <h1 class="title">
          CIFRADO POR BLOQUES
         </h1>
        <h2 class="subtitle">
      Este tipo de cifrado busca reordenar los caracteres del mensaje a cifrar justo con su nombre lo indica, por grupos, para lo cual se consideran grupos de periodo P, lo que implica que la transposición es periódica y que después de aplicar la permutación indicada con el tamaño de grupo dado sobre el texto en claro, esta acción se repetirá tantas veces como sea necesario hasta haber cifrado el texto deseado.
        </h2>
      </div>
    </div>
  </section>

  <form action="" method="POST">
  <section class="section has-background-dark" id="formulario">
  
    <div class="field">
      <label class="label has-text-white">Ingresa tu mensaje.</label>
      <div class="control">
            <textarea class="textarea " name="mensaje" placeholder="Máximo 1000 caracteres" value="${respuesta1}"></textarea>
      </div>
      <div class="field">
    <label class="label has-text-white" for="name">Permutación</label>
  <div class="control">
    <input class="input" type="text" name="permutacion" placeholder="Numeros del 0-9" value="${respuestaPermutacion}"></input>
      </div>
      <div class="select" name="selectorAccion">
        <select>
          <option>Cifrar</option>
          <option>Descifrar</option>
          <option>Atbash</option>
        </select>
      </div>
     <div class="field ">
       <div class="control">
         <button class="button is-warning" id="submitButton" type="submit" >Aceptar</button>
      </div>
    </div>
  <div class="field">
    <label class="label has-text-white">Mensaje cifrado por bloques</label>
      <div class="control">
        <input class="textarea" name="textocifrado" placeholder="" value="${respuestaTextoEncriptado}"></input>
    </div>
  </div>
      </div>
    </section>
  </form>
  
    <footer class="footer">
      <div class="content has-text-centered">
        <p> Keila Gonzalez Luis  </p>
      </div>
    </footer>
    <script src="js/main.js?v6" async defer></script>
</body>