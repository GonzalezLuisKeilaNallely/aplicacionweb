package programacionweb.modelos;


public class ModeloInicio{

    private String cadNormal;
    private String cadEncrip;

    private String perm;
    private int grupo;

    private static final char [] ATBASH = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','ñ','o','p','q','r','s','t','u','v','w','x','y','z'};
    private static final char [] ATBASHCODE =  {'Z','Y','X','W','V','U','T','S','R','Q','P','O','Ñ','N','M','L','K','J','I','H','G','F','E','D','C','B','A'};                               

    public ModeloInicio(String cadNormal, String cadEncrip, String perm, int grupo){
        this.cadNormal=cadNormal;
        this.cadEncrip=cadEncrip;
        this.perm=perm;
        this.grupo=grupo;
     
    }
    public ModeloInicio(){}

    public String limpiarCadena(String cadena){
        String aux="";
        String limpia[]=cadena.split(" ");
        char c;
        for (int i = 0; i < limpia.length; i++) { 
            for(int j=0; j<limpia[i].length();j++){    
             c=limpia[i].charAt(j);  
             aux+=c; 
            }
        } 
            return aux;
    }


    public String limpiarCadena(String[] cadena){
        String aux="";
        String limpia[]=cadena;
        for (int i = 0; i < limpia.length; i++) { 
            aux += limpia[i]+"  ";
        } 
            return aux;
    }

    public String cifrarCadenaBash(String cadena){
        String aux="";
        char c;
        for(int i=0; i<cadena.length(); i++){
            c=cadena.charAt(i);
            for(int j=0; j<ATBASH.length; j++){
                if(c == ATBASH[j]){
                    aux+=ATBASHCODE[j]+"";
                    
                }
            }
        }
            return aux;
    }

    public boolean validacionCadena(String cadena, int perm){
    
        if(cadena.length() % perm==0)
        return true;
        return false;
    }

    
public String[] gruposIguales(String s, int perm)
{
    int len = s.length();

    int nparts = (len + perm - 1) / perm;
    String parts[] = new String[nparts];

    int offset= 0;
    int i = 0;
    while (i < nparts)
    {
        parts[i] = s.substring(offset, Math.min(offset + perm, len));
        offset += perm;
        i++;
    }
    return parts;
}

    public static String cifrado(String texto, String permutacion){
        int periodo = permutacion.length();        
        String mensajeCifrado = "";
        
        int faltantes = texto.length() % periodo;
        for(int i=faltantes; i<=periodo; i++)
            texto += " ";       
        for(int i=0; i<texto.length() - periodo; i+=periodo){
            char[] grupo = texto.substring(i, i + periodo).toCharArray();
            char[] grupoNuevo = new char[periodo];            
            for(int j=0; j<periodo; j++){ 
                int index = Character.getNumericValue(permutacion.charAt(j) - 1);
                grupoNuevo[j] = grupo[index];
            }
            mensajeCifrado += String.valueOf(grupoNuevo);
        } 
        return mensajeCifrado;
    }

    public static String descifrado(String textoCifrado, String permutacion){
        int periodo = permutacion.length();        
        String mensajeDescifrado = "";
                
        for(int i=0; i<textoCifrado.length(); i+=periodo){
            char[] grupo = textoCifrado.substring(i, i + periodo).toCharArray();
            char[] grupoNuevo = new char[periodo];
            for(int j=0; j<periodo; j++){
                int index = Character.getNumericValue(permutacion.charAt(j) - 1); 
                grupoNuevo[index] = grupo[j];
            }     
            mensajeDescifrado += String.valueOf(grupoNuevo);
        }   
        
        return mensajeDescifrado;        
    }    

public static void main(String []args){
    //prueba
    ModeloInicio m= new ModeloInicio();
    String x="ANITALAVALATINA";

    System.out.println(m.validacionCadena("ANITALAVALATINA", 5));
    System.out.println(m.limpiarCadena((m.gruposIguales("ANITALAVALATINA", 5))));
    System.out.println(descifrado(m.limpiarCadena("nedloa limnea nteriz"), "425136"));
    System.out.println(m.cifrarCadenaBash("danielmartinezleon"));
}
public String getCadNormal() {
    return cadNormal;
}
public void setCadNormal(String cadNormal) {
    this.cadNormal = cadNormal;
}
public String getCadEncrip() {
    return cadEncrip;
}
public void setCadEncrip(String cadEncrip) {
    this.cadEncrip = cadEncrip;
}
public String getPerm() {
    return perm;
}
public void setPerm(String perm) {
    this.perm = perm;
}
public int getGrupo() {
    return grupo;
}
public void setGrupo(int grupo) {
    this.grupo = grupo;
}

}