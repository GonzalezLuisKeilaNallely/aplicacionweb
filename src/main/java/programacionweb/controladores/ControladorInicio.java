package programacionweb.controladores;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import programacionweb.modelos.ModeloInicio;

@WebServlet("/inicio")
public class ControladorInicio extends HttpServlet {
   ModeloInicio modelo;
   String cadena="";
  protected void doGet(HttpServletRequest solicitud, HttpServletResponse respuesta)
      throws ServletException, IOException {
    
    solicitud.setAttribute("datos","");
    
    var contextoServlet = solicitud.getServletContext();
    var despachadorSolicitud =
        contextoServlet.getRequestDispatcher("/WEB-INF/vistas/VistaInicio.jsp");
    despachadorSolicitud.forward(solicitud, respuesta);
  }
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
              modelo = new ModeloInicio();
              String c="";
              
              modelo.setCadNormal(request.getParameter("mensaje"));
              modelo.setPerm(request.getParameter("permutacion"));
              c=request.getParameter("selectorAccion");

              modelo.setGrupo(modelo.getPerm().length());
              cadena=modelo.limpiarCadena(modelo.getCadNormal());
             if(request.getParameter("SelectorAccion")=="Atbash"){
                modelo.setCadNormal(modelo.cifrarCadenaBash(cadena));
                request.setAttribute("respuestaTextoEncriptado", modelo.getCadNormal());
              } else if(modelo.validacionCadena(cadena, modelo.getGrupo())){
                request.setAttribute("respuestaPermutacion", modelo.getPerm());
                  if(request.getParameter("selectorAccion")=="Cifrar"){
                      modelo.setCadEncrip(ModeloInicio.cifrado(cadena, modelo.getPerm()));
                      modelo.setCadEncrip(modelo.limpiarCadena(modelo.gruposIguales(modelo.getCadEncrip(),modelo.getGrupo())));
                      request.setAttribute("respuestaTextoEncriptado", modelo.getCadEncrip());
                    }else if(request.getParameter("SelectorAccion")=="Descifrar"){
                      modelo.setCadNormal(ModeloInicio.descifrado(cadena, modelo.getPerm()));
                      modelo.setCadNormal(modelo.limpiarCadena(modelo.gruposIguales(modelo.getCadNormal(),modelo.getGrupo())));
                      request.setAttribute("respuestaTextoEncriptado", modelo.getCadNormal());
                    }
                
           }else{
                c="Permutación ínvalida, prueba de nuevo.";
                request.setAttribute("respuestaPermutacion", c);
              }
              //request.setAttribute("respuestaPermutacion", modelo.getPerm());
              

              doGet(request, response);
              processRequest(request, response);
            }
  private void processRequest(HttpServletRequest request, HttpServletResponse response) {
  }
}